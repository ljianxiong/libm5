#include "cWdfs.h"
#include "../libm5/global.h"
#include "../lib32/efolder.h"

cWdfs::cWdfs()
{
	_currSmaps = new cReader;
	_currReader = new cReader;
	_currSmaps->loadRoot(g_169Path + "smap.wdf");


	auto game = cGame::getInstance();
	switch (game->getui()) {
	case euiType::jd:
		break;
	case euiType::gy:
		_currReader->loadRoot(g_WdfPath + "uigy.wdf");
		break;
	case euiType::sj:
		_currReader->loadRoot(g_WdfPath + "uisj.wdf");
		break;
	case euiType::yz:
		_currReader->loadRoot(g_WdfPath + "uiyz.wdf");
		break;
	}

	auto names = cc::efolder(g_169Path, false, 1);
	for (const auto& name : names){
		if (name.find(".wd") == std::string::npos)   continue;
		if (name.find("smap") != std::string::npos)  continue;
		if (name.find("ui") != std::string::npos)    continue;
		if (name.find("scene") != std::string::npos) continue;
		if (name.find("sound") != std::string::npos) continue;
		if (name.find("music") != std::string::npos) continue;
		if (name.find("stock") != std::string::npos) continue;
		if (name.find("chat") != std::string::npos)  continue;
		if (name.find("mhimage") != std::string::npos) continue;
		if (name.find("wzimage") != std::string::npos) continue;
		if (name.find("goods.wdf") != std::string::npos)  continue;
		if (name.find("addon.wdf") != std::string::npos && name.find("waddon.wdf") == std::string::npos)  continue;
		_currReader->loadRoot(name);
	}
	_currReader->loadRoot(g_169Path + "addon.wdf");
	_currReader->loadRoot(g_169Path + "goods.wdf");

#if !defined(ccc_date)
	_currReader->loadRoot(g_WdfPath + "chiyou.wd5");
#else
	_currReader->loadRoot(g_WdfPath + "yutu.wdf");
	_currReader->loadRoot(g_WdfPath + "yutu.wd5");
#endif
	_currReader->loadRoot(g_WdfPath + "ui.wd5");
	_currReader->loadRoot(g_WdfPath + "arrow.wdf");
}


cWdfs* cWdfs::getInstance()
{
	static cWdfs* s_group = new cWdfs();
	return s_group;
}